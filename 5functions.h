#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

double gridcalc(double *messdaten);

void z_calc(double *messdaten, double *z_coordinates);

double cluster_anzahl(double *z_coordinates, double grid);

uint32_t *cluster(double *z_coordinates, double grid, uint32_t *color_out);


#endif


