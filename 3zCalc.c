#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "5functions.h"

int compare(const void *a, const void *b)
{
    double ax = *(const double *)a;
    double bx = *(const double *)b;

    if (ax < bx)
        return -1;
    if (ax > bx)
        return 1;
    return 0;
}

void z_calc(double *messdaten, double *z_coordinates)
{
    double grid = gridcalc(messdaten);
    int z_column = 2;
    int column = 3;

    for (int i = 0; i < grid ; i++)
    {
        z_coordinates[i] = messdaten[(i+2) * column + z_column];
        printf(" Z[%d]  = %lf \n", i, z_coordinates[i]);
    }

    qsort(z_coordinates, grid , sizeof(double), compare);
    
    for (int i = 0; i < grid ; i++)
    {
        printf(" NEU_Z[%d]  = %lf \n", i, z_coordinates[i]);
    }
}
