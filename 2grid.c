#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "5functions.h"

double gridcalc(double *messdaten)
{
    double x_steps = 0;
    for (double i = messdaten[0]; i <= messdaten[1]; i += messdaten[2])
    {
        x_steps++;
    }
    printf(" x_steps: %lf\n", x_steps);

    double y_steps = 0;
    for (double i = messdaten[3]; i <= messdaten[4]; i += messdaten[5])
    {
        y_steps++;
    }
    printf(" y-steps: %lf\n", y_steps);

    double grid = x_steps * y_steps;
    printf(" Anzahl der Gridpunkte / Groesse des Grids: %lf\n", grid);

    return grid;
}