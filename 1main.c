#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "7bmp.h"
#include "5functions.h"

int main(int argn, char *argv[])
{
    if (argn != 2)
    {
        printf(" usage: %s <string>\n", argv[0]);
        return 1;
    }

    double *messdaten = (double *)malloc(2000000 * sizeof(double));
    if (NULL == messdaten)
    {
        fprintf(stderr, " Cannot allocate memory !Bye !\n");
        exit(1);
    }

    FILE *file = fopen(argv[1], "r");
    if (file == NULL)
    {
        printf(" Datei kann nicht geoeffnet werden\n");
        return 1;
    }

    unsigned N = 2;
    int result;
    result = fscanf(file, "%lf,%lf,%lf\n", &messdaten[N - 2], &messdaten[N - 1], &messdaten[N]);
    while (result != EOF)
    {
        printf(" %lf, %lf, %lf\n", messdaten[N - 2], messdaten[N - 1], messdaten[N]);
        N = N + 3;
        result = fscanf(file, "%lf,%lf,%lf\n", &messdaten[N - 2], &messdaten[N - 1], &messdaten[N]);
    }

    fclose(file);

    double *z_coordinates = (double *)malloc(2000000 * sizeof(double));

    double grid = gridcalc(messdaten);

    z_calc(messdaten, z_coordinates);

    // cluster(messdaten);

    // printf("clusterAnzahl = %lf\n", cluster_anzahl(z_coordinates, grid));

    // cluster(z_coordinates, grid);

    uint32_t *color_out = (uint32_t *)malloc(grid * sizeof(uint32_t));

    bmp_create("heatmap.bmp", cluster(z_coordinates, grid, color_out), 100, 100);

    free(messdaten);
    free(z_coordinates);
    free(color_out);

    return 0;
}
