#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include "5functions.h"

#define COLOR_RANGE 256

double cluster_anzahl(double *z_coordinates, double grid)
{
    double schritte = grid / 10.0;
    double anzahl = 0;
    if (schritte < 10)
    {
        anzahl = grid;
    }
    else
    {
        anzahl = 10;
    }
    return anzahl;
}

uint32_t *cluster(double *z_coordinates, double grid, uint32_t *color_out)
{
    double min_z = z_coordinates[0];
    double max_z = z_coordinates[(int)(grid - 1)];
    printf("min_z = %lf \n max_z = %lf\n", min_z, max_z);
    double anzahl = cluster_anzahl(z_coordinates, grid);

    double cluster_große = ((max_z) - (min_z)) / anzahl;
    printf("cluster_große = %lf\n", cluster_große);
    double cluster_bounds[(int)anzahl];
    double bound = min_z;

    if (grid > 10)
    {
        for (int i = 0; i < anzahl; i++)
        {
            cluster_bounds[i] = bound + i * cluster_große;
            printf("cluster bounds[%d] = %lf\n", i, cluster_bounds[i]);
        }
    }
    else if (grid <= 10)
    {
        for (int i = 0; i < anzahl; i++)
        {
            cluster_bounds[i] = z_coordinates[i];
            printf("cluster bounds[%d] = %lf\n", i, cluster_bounds[i]);
        }
        }

    // uint32_t *color_out = (uint32_t *)malloc(grid * sizeof(uint32_t));
    for (int i = 0; i < grid; i++)
    {
        for (int j = 0; j < anzahl; j++)
        {
            if (z_coordinates[i] < cluster_bounds[j + 1])
            {
                color_out[i] = (j * COLOR_RANGE / (int)anzahl) << 16 | (j * COLOR_RANGE / (int)anzahl) << 8 | (j * COLOR_RANGE / (int)anzahl);
                break;
            }
        }
    }
    return color_out;
}